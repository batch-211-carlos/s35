const express = require("express");

//Mongoose is a package that allows creation of schemas to model our data structure
//Also has access to a number of methods for manipulationg our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//Setup for allowing the server to handle data from requests
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//MongoDB connection
//connect to the database by passing in our connection string, remember to replace the password and database name with actual values
//{newUrlParser:true} allows us to avoid any current and future errors while connecting to MongoDB

//Syntax
	//mongoose.connect("<MongoDB connection string>",{useNewUrlParser:true})

//Connecting to MongoDB Atlas

mongoose.connect("mongodb+srv://admin123:admin123@project0.ynomztd.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
	});

//Set notification for connection success or failure
//connection to the database
//allows us to handle errors when the initial connection is established
//works with on and once Mongoose method
let db = mongoose.connection;
//if a connection occured, output in the console
//console.error.bind(console) allows us to print errors in our terminal or browser console
db.on("error",console.error.bind(console,"connection error"));

//
db.once("open",()=>console.log("We're connected to the cloud database"))

//Mongoose Schemas
//Schemas determine the structure of the documents to be writtenin the database
//Acts as the blueprint of the database
//use the Schema() constructor of the Mongoose module to create a new schema object
//The "new" keyword creates a brand new schema
const taskSchema = new mongoose.Schema({
		//define the fields with their corresponding data type

	name: String,
	password: String,
	status: {
		type: String,
		default: "pending"
	}
})

//Models
//uses Schemas and are used to create/instantiate objects that correspond to the schema
//Server?>Schema(blueprint)>Database>Collection
//Model must be in singular form and capitalize as their naming convention
const Task = mongoose.model("Task",taskSchema);
//first parameter is the mongoose.model which is "Task" and the 2nd parameter is mongoose Schema which "taskSchema"

//Create a new task
//Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		-if the task already exist in the database, we return an error 
		-if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object 
*/

app.post("/tasks",(req,res)=>{

	Task.findOne({name:req.body.name},(err,result)=>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate user was found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("Registered New user"); //201 means ok or request has been fulfilled
				}
			})
		}
	})
})

//Getting all the tasks
//Business logic
/*
1. Retreive all documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks",(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

//Activity
//Signup and duplicate
app.post("/signup",(req,res)=>{

	Task.findOne({name:req.body.username}, {name:req.body.password},(err,result)=>{
		if(request.body.username !== '' && request.body.password !== ''){
			return res.send("Duplicate user was found");
		} else {
			let newTask1 = new Task1({
				username: req.body.username,
				password: req.body.password
			});
			newTask1.save((saveErr,savedTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("Registered New user"); //201 means ok or request has been fulfilled
				}
			})
		}
	})
})

//Stretch Goal
/*app.get("/tasks",(req,res)=>{
	Task.find({},function(err,result){
    var userMap = {};

    user.forEach(function(user) {
      userMap[user._id] = user;
    });

    res.send(userMap);  
  });
});*/

	


app.listen(port,() => console.log(`Server running at port ${port}`))